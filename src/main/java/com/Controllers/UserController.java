package com.Controllers;

import com.Entities.User;
import com.Repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public Optional<User> getUserById(@PathVariable Integer id) {
        return userRepository.findById(id);

    }
}
package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtmIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtmIntegrationApplication.class, args);
	}

}

package com.Entities;


import javax.persistence.*;

@Entity
@Table(name="USER")
@TableGenerator(name="tab", initialValue=0, allocationSize=50)
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="tab")
    Integer user_id;
    String user;

    public User(Integer user_id, String user) {
        this.user_id = user_id;
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
